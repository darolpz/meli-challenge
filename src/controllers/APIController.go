package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"../models"
	"../services"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

//HomeEndpoint endpoint del home
func HomeEndpoint(w http.ResponseWriter, req *http.Request) {
	db, err := gorm.Open("mysql", "meli_challenge:planetas2019@/meli_challenge?charset=utf8&parseTime=True&loc=Local")
	defer db.Close()
	if err != nil {
		fmt.Println(err)
	}
	var vulcanus, ferengis, betasoides models.Planet
	db.Where("id = ?", 1).First(&vulcanus)
	db.Where("id = ?", 2).First(&ferengis)
	db.Where("id = ?", 3).First(&betasoides)
	layout := "2006-01-02"
	str := "2019-09-16"
	initDate, err := time.Parse(layout, str)
	if err != nil {
		fmt.Println(err)
	}
	last := initDate.AddDate(10, 0, 0)
	totalDays := int(last.Sub(initDate).Hours() / 24)
	var index, rainyDaysCounter, rainyPeriodCounter, droughtCounter, optimalConditionCounter, maxRainyDay, previousPeriodFlag int
	var maxPerimeter float64
	for index <= totalDays {
		date := initDate.AddDate(0, 0, index)
		vulcanusPosition := vulcanus.CalculatePosition(90, index)
		vulcanusPosition.Date = &date
		ferengisPosition := ferengis.CalculatePosition(90, index)
		ferengisPosition.Date = &date
		betasoidesPosition := betasoides.CalculatePosition(90, index)
		betasoidesPosition.Date = &date

		// Para que el periodo de la ultima iteracion no quede con uno menos
		if index == totalDays {
			if previousPeriodFlag == 1 {
				rainyPeriodCounter++
			}
			if previousPeriodFlag == 2 {
				droughtCounter++
			}
			if previousPeriodFlag == 3 {
				optimalConditionCounter++
			}
		}
		// Si el sol esta dentro del triangulo se ejecuta el bloque dentro de if y se prosigue con la siguiente iteracion
		//Es el periodo mas comun
		sunInsideTriangle := services.IsSunInsideTriangle(vulcanusPosition, ferengisPosition, betasoidesPosition)
		if sunInsideTriangle {
			perimeter := services.GetPerimeter(vulcanusPosition, ferengisPosition, betasoidesPosition)
			if perimeter > maxPerimeter {
				maxPerimeter = perimeter
				maxRainyDay = index
			}
			if previousPeriodFlag == 2 {
				droughtCounter++
			}
			if previousPeriodFlag == 3 {
				optimalConditionCounter++
			}
			previousPeriodFlag = 1
			rainyDaysCounter++
			index++
			continue
		}
		// Si los tres planetas estan alineados con el sol se ejecuta el bloque dentro de if y se prosigue con la siguiente iteracion
		alignedWithSun := services.ArePositionsAlignedWithSun(vulcanusPosition, ferengisPosition, betasoidesPosition)
		if alignedWithSun {
			if previousPeriodFlag == 1 {
				rainyPeriodCounter++
			}
			if previousPeriodFlag == 3 {
				optimalConditionCounter++
			}
			previousPeriodFlag = 2
			index++
			continue
		}
		// Si los tres planetas estan alineados entre si pero no con el sol se ejecuta el bloque dentro de if y se prosigue con la siguiente iteracion
		arePositionsAligned := services.ArePositionsAligned(vulcanusPosition, ferengisPosition, betasoidesPosition)
		if arePositionsAligned {
			if previousPeriodFlag == 1 {
				rainyPeriodCounter++
			}
			if previousPeriodFlag == 2 {
				droughtCounter++
			}
			previousPeriodFlag = 3
			index++
			continue
		}

		if previousPeriodFlag == 1 {
			rainyPeriodCounter++
		}
		if previousPeriodFlag == 2 {
			droughtCounter++
		}
		if previousPeriodFlag == 3 {
			optimalConditionCounter++
		}
		previousPeriodFlag = 0
		index++
	}

	response := struct {
		Status            int    `json:"status"`
		Message           string `json:"message"`
		PeriodosDeSequia  int    `json:"cantidad_periodos_sequia"`
		PeriodosDeLluvia  int    `json:"cantidad_periodos_lluvia"`
		DiasDeLluvia      int    `json:"cantidad_dias_lluvia"`
		DatePicoDeLLuvia  string `json:"pico_lluvia"`
		IndexPicoDeLLuvia int    `json:"pico_lluvia_indice"`
		PeriodosOptimos   int    `json:"cantidad_periodos_optimos"`
	}{
		Status:            200,
		Message:           "Data got succesfully",
		PeriodosDeSequia:  droughtCounter,
		PeriodosDeLluvia:  rainyPeriodCounter,
		DiasDeLluvia:      rainyDaysCounter,
		DatePicoDeLLuvia:  initDate.AddDate(0, 0, maxRainyDay).Format("2006-01-02"),
		PeriodosOptimos:   optimalConditionCounter,
		IndexPicoDeLLuvia: maxRainyDay,
	}

	json.NewEncoder(w).Encode(response)
}

//GetForecastEndpoint endpoint del home
func GetForecastEndpoint(w http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	db, err := gorm.Open("mysql", "meli_challenge:planetas2019@/meli_challenge?charset=utf8&parseTime=True&loc=Local")
	defer db.Close()
	if err != nil {
		fmt.Println(err)
	}
	var forecast models.Forecast
	id, _ := strconv.Atoi(params["day"])
	var response struct {
		Status   int    `json:"status"`
		Message  string `json:"message"`
		Date     string `json:"fecha"`
		DayIndex int    `json:"indice"`
		Weather  string `json:"clima"`
	}
	if db.Where("id = ?", id+1).First(&forecast).RecordNotFound() {
		response.Status = 400
		response.Message = "Forecast not found"

	} else {
		response.Status = 200
		response.Message = "Forecast got succesfully"
		response.Date = forecast.Date.Format("2006-01-02")
		response.DayIndex = forecast.Day
		response.Weather = forecast.Weather
	}

	json.NewEncoder(w).Encode(response)
}
