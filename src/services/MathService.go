package services

import (
	"math"

	"../models"
)

// ArePositionsAlignedWithSun devuelve true si las 3 posiciones estan alineadas con el sol (tres planetas con el mismo angulo o +180°)
func ArePositionsAlignedWithSun(posA, posB, posC *models.Position) bool {
	inversePositionB := posB.Degree + 180
	if inversePositionB > 360 {
		inversePositionB = inversePositionB % 360
	}

	inversePositionC := posC.Degree + 180
	if inversePositionC > 360 {
		inversePositionC = inversePositionC % 360
	}
	return (posA.Degree == posB.Degree || posA.Degree == inversePositionB) && (posA.Degree == posC.Degree || posA.Degree == inversePositionC)
}

// IsSunInsideTriangle devuelve true si el punto 0 (sol) esta dentro del triangulo formado por los 3 puntos
// Si todas las orientaciones tienen el mismo signo entonces el punto 0 esta dentro del triangulo
func IsSunInsideTriangle(posA, posB, posC *models.Position) bool {
	point0 := Point{X: 0, Y: 0}
	pointA := Point{X: posA.X, Y: posA.Y}
	pointB := Point{X: posB.X, Y: posB.Y}
	pointC := Point{X: posC.X, Y: posC.Y}

	mainTriangleOrientation := GetTriangleOrientation(pointA, pointB, pointC)

	triangleAB0 := GetTriangleOrientation(pointA, pointB, point0)
	triangleA0C := GetTriangleOrientation(pointA, point0, pointC)
	triangle0BC := GetTriangleOrientation(point0, pointB, pointC)

	if mainTriangleOrientation > 0 {
		return triangleAB0 > 0 && triangleA0C > 0 && triangle0BC > 0
	}
	/* (Else) */
	return triangleAB0 < 0 && triangleA0C < 0 && triangle0BC < 0

}

// GetTriangleOrientation devuelve la orientacion del triangulo
func GetTriangleOrientation(posA, posB, posC Point) int {
	return int((posA.X-posC.X)*(posB.Y-posC.Y) - (posA.Y-posC.Y)*(posB.X-posC.X))
}

// GetPerimeter es usada para obtener el perimetro del triangulo formado entre el sol y los 3 planetas
func GetPerimeter(pointA, pointB, pointC *models.Position) float64 {
	distanceAB := GetHipotenusa(pointA, pointB)
	distanceBC := GetHipotenusa(pointB, pointC)
	distanceAC := GetHipotenusa(pointA, pointC)
	return distanceAB + distanceBC + distanceAC
}

// GetHipotenusa es una funcion auxiliar de get perimeter
func GetHipotenusa(pointA, pointB *models.Position) float64 {
	catetoA := (pointB.X - pointA.X) * (pointB.X - pointA.X)
	catetoB := (pointB.Y - pointA.Y) * (pointB.Y - pointA.Y)
	hipotenusa := math.Sqrt(catetoA + catetoB)
	return hipotenusa
}

// Point estructura auxiliar para realizar calculo de orientacion de triangulo
type Point struct {
	X float64
	Y float64
}

// ArePositionsAligned Devuelve true si los 3 planetas estan alineados
func ArePositionsAligned(posA, posB, posC *models.Position) bool {
	pendiente := (posB.Y - posA.Y) / (posB.X - posA.X)
	ordenada := pendiente*-1*posA.X + posA.Y
	bool := pendiente*posC.X+ordenada == posC.Y
	return bool
}
