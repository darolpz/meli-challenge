package models

import (
	"math"
	"time"
)

// Planet ...
type Planet struct {
	ID              uint `gorm:"primary_key"`
	Name            string
	Distance        int // in kilometers
	Positions       []Position
	AngularVelocity int // degrees per day
	CreatedAt       time.Time
	UpdatedAt       time.Time
	DeletedAt       *time.Time
}

/*
CalculatePosition calcular posicion en x,y y grados respecto del sol en cada dia
*/
func (planet *Planet) CalculatePosition(initDegree int, dayNumber int) *Position {
	degrees := initDegree + (dayNumber * planet.AngularVelocity)
	if degrees > 360 {
		degrees = degrees % 360
	}
	/*
		Regla de 3:
		Si 360° = 2Pi
		entonces
		x° = nPi
		entonces
		nPi = x° * 2 / 360.0 (360,0 para que devuelva flotante)
	*/
	var radians float64
	radians = float64(degrees) * 2 / 360.0
	sin, cos := math.Sincos(math.Pi * radians)
	x := cos * float64(planet.Distance)
	y := sin * float64(planet.Distance)
	roundedX := math.Round(x*10000) / 10000
	roundedY := math.Round(y*10000) / 10000
	/* fmt.Printf("Degrees: %v\n", degrees)
	fmt.Printf("x: %v, y: %v \n", x, y)
	fmt.Printf("pitagoras: %v\n", math.Sqrt((x*x)+(y*y)))
	fmt.Println("") */

	return &Position{
		PlanetID: planet.ID,
		Degree:   degrees,
		X:        roundedX,
		Y:        roundedY,
	}
}
