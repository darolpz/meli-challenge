package models

import "time"

// Forecast es la estructura usada para guardar dias ...
type Forecast struct {
	ID        uint       `gorm:"primary_key"`
	Date      *time.Time `gorm:"type:date"`
	Day       int
	Weather   string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
