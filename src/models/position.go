package models

import "time"

// Position ...
type Position struct {
	ID        uint       `gorm:"primary_key"`
	Date      *time.Time `gorm:"type:date"`
	Degree    int
	X         float64
	Y         float64
	PlanetID  uint
	Planet    Planet
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}
