package main

import (
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	"./controllers"
	"./models"
	"./services"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	//Inicializacion
	router := mux.NewRouter()
	start := time.Now()
	db, err := gorm.Open("mysql", "meli_challenge:planetas2019@/meli_challenge?charset=utf8&parseTime=True&loc=Local")
	defer db.Close()
	if err != nil {
		fmt.Println(err)
	}

	db.DropTable(&models.Planet{}, &models.Position{}, &models.Forecast{})
	db.AutoMigrate(&models.Planet{}, &models.Position{}, &models.Forecast{})
	vulcanus := models.Planet{Name: "Vulcanos", Distance: 500, AngularVelocity: 359}
	ferengis := models.Planet{Name: "Ferengis", Distance: 2000, AngularVelocity: 357}
	betasoides := models.Planet{Name: "Betasoides", Distance: 1000, AngularVelocity: 5}

	db.Create(&vulcanus)
	db.Create(&ferengis)
	db.Create(&betasoides)
	//Inicio programa principal
	//Inicializacion de variables
	layout := "2006-01-02"
	str := "2019-09-16"
	initDate, err := time.Parse(layout, str)
	if err != nil {
		fmt.Println(err)
	}
	last := initDate.AddDate(10, 0, 0)

	totalDays := int(last.Sub(initDate).Hours() / 24)
	var index, rainyDaysCounter, rainyPeriodCounter, droughtCounter, optimalConditionCounter, maxRainyDay, previousPeriodFlag int
	var maxPerimeter float64

	//Inicio Workers
	jobsPosition := make(chan *models.Position, (totalDays+1)*3)
	jobsForecast := make(chan *models.Forecast, (totalDays+1)*3)
	var waitGroupPosition, waitGroupForecast sync.WaitGroup
	waitGroupPosition.Add((totalDays + 1) * 3)
	waitGroupForecast.Add((totalDays + 1))

	go func(jobsPosition <-chan *models.Position) {
		for position := range jobsPosition {
			defer waitGroupPosition.Done()
			db.Create(&position)
		}
	}(jobsPosition)

	go func(jobsForecast <-chan *models.Forecast) {
		for forecast := range jobsForecast {
			defer waitGroupForecast.Done()
			db.Create(&forecast)
		}
	}(jobsForecast)

	//Main loop
	for index <= totalDays {
		date := initDate.AddDate(0, 0, index)
		vulcanusPosition := vulcanus.CalculatePosition(90, index)
		vulcanusPosition.Date = &date
		ferengisPosition := ferengis.CalculatePosition(90, index)
		ferengisPosition.Date = &date
		betasoidesPosition := betasoides.CalculatePosition(90, index)
		betasoidesPosition.Date = &date

		forecast := models.Forecast{Date: &date, Day: index}
		jobsPosition <- vulcanusPosition
		jobsPosition <- ferengisPosition
		jobsPosition <- betasoidesPosition
		// Para que el periodo de la ultima iteracion no quede con uno menos
		if index == totalDays {
			if previousPeriodFlag == 1 {
				rainyPeriodCounter++
			}
			if previousPeriodFlag == 2 {
				droughtCounter++
			}
			if previousPeriodFlag == 3 {
				optimalConditionCounter++
			}
		}

		// Si el sol esta dentro del triangulo se ejecuta el bloque dentro de if y se prosigue con la siguiente iteracion
		//Es el periodo mas comun
		sunInsideTriangle := services.IsSunInsideTriangle(vulcanusPosition, ferengisPosition, betasoidesPosition)
		if sunInsideTriangle {
			perimeter := services.GetPerimeter(vulcanusPosition, ferengisPosition, betasoidesPosition)
			if perimeter > maxPerimeter {
				maxPerimeter = perimeter
				maxRainyDay = index
			}
			if previousPeriodFlag == 2 {
				droughtCounter++
			}
			if previousPeriodFlag == 3 {
				optimalConditionCounter++
			}
			forecast.Weather = "Lluvioso"
			rainyDaysCounter++
			jobsForecast <- &forecast
			index++
			previousPeriodFlag = 1
			continue

		}
		// Si los tres planetas estan alineados con el sol se ejecuta el bloque dentro de if y se prosigue con la siguiente iteracion
		alignedWithSun := services.ArePositionsAlignedWithSun(vulcanusPosition, ferengisPosition, betasoidesPosition)
		if alignedWithSun {
			if previousPeriodFlag == 1 {
				rainyPeriodCounter++
			}
			if previousPeriodFlag == 3 {
				optimalConditionCounter++
			}
			forecast.Weather = "Soleado(sequía)"
			//droughtCounter++
			jobsForecast <- &forecast
			index++
			previousPeriodFlag = 2
			continue
		}

		// Si los tres planetas estan alineados entre si pero no con el sol se ejecuta el bloque dentro de if y se prosigue con la siguiente iteracion
		arePositionsAligned := services.ArePositionsAligned(vulcanusPosition, ferengisPosition, betasoidesPosition)
		if arePositionsAligned {
			if previousPeriodFlag == 1 {
				rainyPeriodCounter++
			}
			if previousPeriodFlag == 2 {
				droughtCounter++
			}
			//optimalConditionCounter++
			previousPeriodFlag = 3
			forecast.Weather = "Condiciones optimas de presión y temperatura"
			jobsForecast <- &forecast
			index++
			continue
		}
		if previousPeriodFlag == 1 {
			rainyPeriodCounter++
		}
		if previousPeriodFlag == 2 {
			droughtCounter++
		}
		if previousPeriodFlag == 3 {
			optimalConditionCounter++
		}

		previousPeriodFlag = 0
		forecast.Weather = "Normal"
		jobsForecast <- &forecast
		index++
	}
	// Cierre de channels
	close(jobsPosition)
	close(jobsForecast)

	dbStart := time.Now()
	fmt.Printf("Tiempo transcurrido %s\n", time.Since(start))
	fmt.Printf("Cantidad de periodos de sequia: %v\n", droughtCounter)
	fmt.Printf("Cantidad de periodos de lluvia: %v\n", rainyPeriodCounter)
	fmt.Printf("Cantidad de dias de lluvia: %v\n", rainyDaysCounter)
	fmt.Printf("Pico de lluvia maximo el dia: %v\n", initDate.AddDate(0, 0, maxRainyDay).Format("2006-01-02"))
	fmt.Printf("Pico de lluvia maximo el dia: %v\n", maxRainyDay)
	fmt.Printf("Cantidad de periodos de condiciones optimas de presion y temperatura: %v\n", optimalConditionCounter)
	fmt.Println("Levantando servidor...")
	waitGroupPosition.Wait()
	waitGroupForecast.Wait()
	fmt.Println("Servidor listo")
	fmt.Printf("Tiempo transcurrido %s\n", time.Since(dbStart))
	router.HandleFunc("/", controllers.HomeEndpoint).Methods("GET")
	router.HandleFunc("/clima", controllers.GetForecastEndpoint).Queries("dia", "{day}").Methods("GET")
	log.Fatal(http.ListenAndServe(":8080", router))

}
